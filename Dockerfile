FROM centos/python-38-centos7

USER 0
WORKDIR /python_api

COPY requirements.txt /python_api/requirements.txt

RUN yum -y upgrade \
  && yum -y clean all \
  && rm -rf /var/cache \
  && /opt/app-root/bin/python3.8 -m pip install --upgrade pip \
  && pip3 install -r requirements.txt \
  && rm -f requirements.txt

COPY ./app/python-api.py /python_api/python-api.py

RUN chown -R 1001:0 /python_api
EXPOSE 5290/tcp
USER 1001

ENTRYPOINT ["python3.8"]
CMD ["python-api.py"]
